package com.atlassian.upm.pageobjects;

import java.io.File;
import java.net.URI;

import javax.inject.Inject;

import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.webdriver.AtlassianWebDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.atlassian.upm.pageobjects.Predicates.whereText;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.find;

public final class UploadPluginDialog
{
    private static final String UPLOAD_DIALOG_ID = "upm-upload-dialog";
    static final By UPLOAD_DIALOG = By.id(UPLOAD_DIALOG_ID);

    private @Inject AtlassianWebDriver driver;

    @FindBy(id=UPLOAD_DIALOG_ID)
    private WebElement dialog;

    @FindBy(id="upm-upload-url")
    private WebElement uploadUriInput;

    @FindBy(id="upm-upload-file")
    private WebElement uploadFileInput;

    @WaitUntil
    public void waitUntilDialogIsDisplayed()
    {
        driver.waitUntilElementIsVisible(UPLOAD_DIALOG);
    }

    public void uploadFromUri(URI uri)
    {
        uploadUriInput.sendKeys(uri.toASCIIString());
        find(dialog.findElements(By.tagName("button")), whereText(equalTo("Upload"))).click();
        driver.waitUntilElementIsNotVisible(UPLOAD_DIALOG);
    }

    public void uploadFile(File pluginFile)
    {
        uploadFileInput.sendKeys(pluginFile.getAbsolutePath());
        find(dialog.findElements(By.tagName("button")), whereText(equalTo("Upload"))).click();
        driver.waitUntilElementIsNotVisible(UPLOAD_DIALOG);
    }
}