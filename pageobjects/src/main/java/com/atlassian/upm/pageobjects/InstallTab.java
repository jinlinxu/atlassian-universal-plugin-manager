package com.atlassian.upm.pageobjects;

import java.io.File;
import java.net.URI;

import javax.inject.Inject;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.webdriver.AtlassianWebDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import static com.atlassian.upm.pageobjects.UploadPluginDialog.UPLOAD_DIALOG;
import static com.atlassian.upm.pageobjects.Waits.elementIsVisible;
import static com.atlassian.upm.pageobjects.Waits.loaded;
import static com.atlassian.upm.pageobjects.Waits.or;
import static com.atlassian.upm.pageobjects.Waits.selectedAndLoaded;
import static com.atlassian.upm.pageobjects.WebElements.findPluginElement;
import static com.google.common.base.Suppliers.ofInstance;

public class InstallTab extends UPMTab
{
    private static final String FEATURED_CONTAINER_ID = "upm-install-container-featured";
    private static final By FEATURED_CONTAINER = By.id(FEATURED_CONTAINER_ID);
    private static final String POPULAR_CONTAINER_ID = "upm-install-container-popular";
    private static final By POPULAR_CONTAINER = By.id(POPULAR_CONTAINER_ID);
    private static final String AVAILABLE_CONTAINER_ID = "upm-install-container-available";
    private static final By AVAILABLE_CONTAINER = By.id(AVAILABLE_CONTAINER_ID);
    private static final String PENDING_TASKS_SELECTOR = "upm-pending-tasks";
    private static final By PENDING_TASKS = By.id(PENDING_TASKS_SELECTOR);
    private static final String ERROR_MESSAGE_SELECTOR = ".upm-message.error";
    private static final By ERROR_MESSAGE = By.cssSelector(ERROR_MESSAGE_SELECTOR);
    private static final String PROGRESS_DIALOG_ID = "upm-progress";
    private static final By PROGRESS_DIALOG = By.id(PROGRESS_DIALOG_ID);
    private static final String UPLOAD_BUTTON_ID = "upm-upload";
    private static final By UPLOAD_BUTTON = By.id(UPLOAD_BUTTON_ID);
    private static final String SEARCH_BOX_ID = "upm-install-search-box";
    private static final By SEARCH_BOX = By.id(SEARCH_BOX_ID);
    private static final String SEARCH_BUTTON_ID = "upm-install-search-submit-button";
    private static final By SEARCH_BUTTON = By.id(SEARCH_BUTTON_ID);
    private static final String SEARCH_CONTAINER_ID = "upm-install-container-search";
    private static final By SEARCH_CONTAINER = By.id(SEARCH_CONTAINER_ID);
    private static final String SEARCH_RESULTS_SECTION_SELECTOR = "#upm-install-search .upm-plugin-list-container";
    private static final By SEARCH_RESULTS_SECTION = By.cssSelector(SEARCH_RESULTS_SECTION_SELECTOR);

    private @Inject AtlassianWebDriver driver;
    private @Inject PageBinder binder;

    @FindBy(id="upm-panel-install")
    private WebElement installPanel;

    @FindBy(id="upm-upload")
    private WebElement uploadButton;

    @FindBy(id="upm-install-type")
    private WebElement pluginsToShowSelect;

    @FindBy(id="upm-container")
    private WebElement containerDiv;

    @WaitUntil
    public void waitUntilTabIsLoaded()
    {
        driver.waitUntil(selectedAndLoaded(installPanel));
    }

    public UploadPluginDialog openUploadPluginDialog()
    {
        uploadButton.click();
        return binder.bind(UploadPluginDialog.class);
    }

    public WebElement getUploadButton()
    {
        return uploadButton;
    }

    public void waitForUploadButtonToHide()
    {
        driver.waitUntilElementIsNotVisible(UPLOAD_BUTTON);
    }

    public boolean isUploadPluginDialogShowing()
    {
        return driver.elementIsVisible(UPLOAD_DIALOG);
    }

    public boolean isElementShowing(String elementId)
    {
        return driver.elementIsVisible(By.id(elementId));
    }

    public boolean waitUntilProgressDialogShowing()
    {
        driver.waitUntilElementIsVisible(PROGRESS_DIALOG);
        return driver.elementIsVisible(PROGRESS_DIALOG);
    }

    public InstallTab show(Category category)
    {
        new Select(pluginsToShowSelect).selectByValue(category.getOptionValue());
        driver.waitUntil(loaded(driver.findElement(category.getContainerSelector())));
        return this;
    }

    public boolean isShowing(Category category)
    {
        return driver.elementIsVisible(category.getContainerSelector());
    }

    public AvailablePlugin getPlugin(String key)
    {
        return binder.bind(AvailablePlugin.class, findPluginElement(key, "install", ofInstance(installPanel)));
    }

    public Message getErrorMessage()
    {
        return binder.bind(Message.class, ofInstance(containerDiv), ERROR_MESSAGE);
    }

    public String getPendingTasksText()
    {
        driver.waitUntilElementIsVisible(PENDING_TASKS);
        return driver.findElement(PENDING_TASKS).getText();
    }
    
    public void uploadPlugin(URI uri)
    {
        openUploadPluginDialog().
            uploadFromUri(uri);

        driver.waitUntil(or(elementIsVisible(By.className("upm-message"), ofInstance(containerDiv)),
                            elementIsVisible(By.id("upm-requires-restart-message"), ofInstance(containerDiv))));
    }

    public void uploadPlugin(File pluginFile)
    {
        openUploadPluginDialog().
            uploadFile(pluginFile);

        driver.waitUntil(or(elementIsVisible(By.className("upm-message"), ofInstance(containerDiv)),
                            elementIsVisible(By.id("upm-requires-restart-message"), ofInstance(containerDiv))));

    }

    public void uploadSelfUpgradePlugin(File pluginFile)
    {
        openUploadPluginDialog().
            uploadFile(pluginFile);

        driver.waitUntil(or(elementIsVisible(By.className("upm-message"), ofInstance(containerDiv)),
                            elementIsVisible(By.id("upm-current-plugins"), ofInstance(containerDiv))));
    }
    
    public void doSearch(String query)
    {
        driver.waitUntilElementIsVisible(SEARCH_BOX);
        WebElement searchBox = driver.findElement(SEARCH_BOX);
        searchBox.clear();
        searchBox.sendKeys(query);
        WebElement searchButton = driver.findElement(SEARCH_BUTTON);
        searchButton.click();
        driver.waitUntilElementIsVisible(SEARCH_RESULTS_SECTION);
    }

    @Override
    protected String getPluginContainerId(TabCategory category)
    {
        return "#upm-install-container-" + category.getOptionValue();
    }
    
    public enum Category implements TabCategory
    {
        FEATURED("featured", FEATURED_CONTAINER),
        POPULAR("popular", POPULAR_CONTAINER),
        AVAILABLE("available", AVAILABLE_CONTAINER),
        SEARCH("search", SEARCH_CONTAINER);

        private final String optionValue;
        private final By containerSelector;

        private Category(String optionValue, By containerSelector)
        {
            this.optionValue = optionValue;
            this.containerSelector = containerSelector;
        }

        public String getOptionValue()
        {
            return optionValue;
        }

        public By getContainerSelector()
        {
            return containerSelector;
        }

    }
}
