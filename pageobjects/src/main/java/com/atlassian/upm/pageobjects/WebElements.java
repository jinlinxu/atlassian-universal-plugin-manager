package com.atlassian.upm.pageobjects;

import com.google.common.base.Supplier;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static com.atlassian.upm.pageobjects.Ids.createHash;
import static com.atlassian.upm.pageobjects.Suppliers.findElement;

class WebElements
{
    static Supplier<WebElement> findPluginElement(final String key, String type, final Supplier<WebElement> e)
    {
        return findElement(By.id("upm-plugin-" + createHash(key, type)), e);
    }

    static final By PLUGIN = By.className("upm-plugin");
    static final By PLUGIN_ROW = By.className("upm-plugin-row");
    static final By PLUGIN_NAME = By.className("upm-plugin-name");
    static final By UPM_DETAILS = By.className("upm-details");
    static final By MESSAGE = By.className("upm-message");
}
