package com.atlassian.upm.pageobjects;

import com.google.common.base.Supplier;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static com.google.common.base.Suppliers.ofInstance;

public class Suppliers
{
    public static Supplier<WebElement> findElement(final By by, final WebElement e)
    {
        return findElement(by, ofInstance(e));
    }

    public static Supplier<WebElement> findElement(final By by, final Supplier<WebElement> e)
    {
        return new Supplier<WebElement>()
        {
            public WebElement get()
            {
                return e.get().findElement(by);
            }
        };
    }
}
