package com.atlassian.upm.pageobjects;

import java.net.URI;

import javax.inject.Inject;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.webdriver.AtlassianWebDriver;

import com.google.common.base.Supplier;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import static com.atlassian.upm.pageobjects.Suppliers.findElement;
import static com.atlassian.upm.pageobjects.Waits.and;
import static com.atlassian.upm.pageobjects.Waits.elementIsVisible;
import static com.atlassian.upm.pageobjects.Waits.hasTitle;
import static com.atlassian.upm.pageobjects.Waits.loaded;
import static com.atlassian.upm.pageobjects.Waits.or;
import static com.atlassian.upm.pageobjects.WebElements.MESSAGE;
import static com.atlassian.upm.pageobjects.WebElements.UPM_DETAILS;
import static com.google.common.base.Preconditions.checkNotNull;

public class InstalledPluginDetails
{
    private static final By ENABLE_BUTTON = By.className("upm-enable");
    private static final By DISABLE_BUTTON = By.className("upm-disable");
    private static final By UNINSTALL_BUTTON = By.className("upm-uninstall");
    private static final By VERSION = By.className("upm-plugin-version");
    private static final By PLUGIN_DETAILS_LINK_TITLE = By.className("upm-details-link");
    private static final By PLUGIN_DETAILS_LINK = By.cssSelector("span.upm-details-link a");
    private static final By CONFIGURE_LINK = By.className("upm-configure-link");
    private static final By MODULE_COUNT = By.className("upm-count-enabled");
    private static final By STATUS_MESSAGE = By.cssSelector(".upm-details > .upm-message");
    private static final By REQUIRES_RESTART_MESSAGE = By.className("upm-requires-restart-message");
    private static final By MODULES = By.className("upm-plugin-modules");
    private static final By MODULES_PRESENT = By.cssSelector("span.upm-module-present");
    private static final By NO_MODULES_MESSAGE = By.cssSelector("span.upm-module-none");
    private static final By MODULES_LINK = By.className("upm-module-toggle");

    private @Inject AtlassianWebDriver driver;
    private @Inject PageBinder binder;

    private final InstalledPlugin plugin;
    private final Supplier<WebElement> pluginDetails;
    private final Supplier<WebElement> version;
    private final Supplier<WebElement> uninstallButton;
    private final Supplier<WebElement> enableButton;
    private final Supplier<WebElement> disableButton;
    private final Supplier<WebElement> configureLink;
    private final Supplier<WebElement> modules;
    private final Supplier<WebElement> moduleCount;
    private final Supplier<WebElement> moduleLink;

    public InstalledPluginDetails(Supplier<WebElement> pluginDetails, InstalledPlugin plugin)
    {
        this.plugin = checkNotNull(plugin, "plugin");
        this.pluginDetails = checkNotNull(pluginDetails, "pluginDetails");
        this.version = findElement(VERSION, pluginDetails);
        this.uninstallButton = findElement(UNINSTALL_BUTTON, pluginDetails);
        this.enableButton = findElement(ENABLE_BUTTON, pluginDetails);
        this.disableButton = findElement(DISABLE_BUTTON, pluginDetails);
        this.configureLink = findElement(CONFIGURE_LINK, pluginDetails);
        this.modules = findElement(MODULES, pluginDetails);
        this.moduleCount = findElement(MODULE_COUNT, modules);
        this.moduleLink = findElement(MODULES_LINK, modules);
    }

    @WaitUntil
    public void waitUntilPluginDetailsAreLoaded()
    {
        driver.waitUntil(and(elementIsVisible(UPM_DETAILS, plugin.getWebElement()),
                             loaded(pluginDetails.get())));
    }

    @WaitUntil
    public void waitUntilDetailsLinkIsLoaded()
    {
        try
        {
            WebElement detailsLinkSpan = pluginDetails.get().findElement(PLUGIN_DETAILS_LINK_TITLE);
            driver.waitUntil(or(hasTitle(detailsLinkSpan, ""),
                                hasTitle(detailsLinkSpan, "Plugin not found in Atlassian Plugin Exchange.")));
        }
        catch (NoSuchElementException e)
        {
            // If there's no Plugin Details link, keep going.
        }
    }

    public boolean isEnabled()
    {
        return plugin.isEnabled();
    }

    public boolean canBeEnabled()
    {
        return driver.elementIsVisibleAt(ENABLE_BUTTON, pluginDetails.get());
    }

    public Message enable()
    {
        if (isEnabled())
        {
            throw new RuntimeException("Plugin is already enabled");
        }
        if (!canBeEnabled())
        {
            throw new RuntimeException("Plugin cannot be enabled");
        }
        enableButton.get().click();
        driver.waitUntil(plugin.isEnabledF());
        return binder.bind(Message.class, plugin.getWebElement(), STATUS_MESSAGE);
    }

    public boolean isDisabled()
    {
        return plugin.isDisabled();
    }

    public boolean canBeDisabled()
    {
        return driver.elementIsVisibleAt(DISABLE_BUTTON, pluginDetails.get());
    }

    public Message disable()
    {
        if (isDisabled())
        {
            throw new RuntimeException("Plugin is already disabled");
        }
        if (!canBeDisabled())
        {
            throw new RuntimeException("Plugin cannot be disabled");
        }
        disableButton.get().click();
        driver.waitUntil(plugin.isDisabledF());
        return binder.bind(Message.class, plugin.getWebElement(), STATUS_MESSAGE);
    }

    public ConfirmDialog uninstall()
    {
        if (!canBeUninstalled())
        {
            throw new RuntimeException("Plugin cannot be uninstalled");
        }
        uninstallButton.get().click();
        return binder.bind(ConfirmDialog.class);
    }

    public boolean canBeUninstalled()
    {
        return driver.elementIsVisibleAt(UNINSTALL_BUTTON, pluginDetails.get());
    }
    
    public String getVersion()
    {
        return version.get().getText();
    }

    public boolean hasPluginDetailsLink()
    {
        return driver.elementIsVisibleAt(PLUGIN_DETAILS_LINK, pluginDetails.get());
    }

    public Link getConfigureLink()
    {
        return new Link(URI.create(configureLink.get().getAttribute("href")));
    }

    public int getNumberOfModulesEnabled()
    {
        String text = moduleCount.get().getText();
        return Integer.parseInt(text.substring(0, text.indexOf(' ')));
    }

    public boolean requiresRestart()
    {
        return driver.elementIsVisibleAt(REQUIRES_RESTART_MESSAGE, pluginDetails.get());
    }

    public boolean hasModules()
    {
        return driver.elementIsVisibleAt(MODULES_PRESENT, modules.get()) &&
                !driver.elementIsVisibleAt(NO_MODULES_MESSAGE, modules.get());
    }

    public PluginModuleManager openModuleManager()
    {
        if (!hasModules())
        {
            throw new RuntimeException("Plugin does not have modules");
        }
        moduleLink.get().click();
        return binder.bind(PluginModuleManager.class, modules);
    }

    public boolean hasModuleStatusCompletelyHidden()
    {
        return modules.get().getAttribute("class").contains("hidden");
    }

    public WebElement getMessage()
    {
        return plugin.getWebElement().get().findElement(MESSAGE);
    }
}
