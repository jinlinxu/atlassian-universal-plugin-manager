package com.atlassian.upm.pageobjects;

import javax.inject.Inject;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.webdriver.AtlassianWebDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.atlassian.upm.pageobjects.Suppliers.findElement;
import static com.atlassian.upm.pageobjects.Waits.and;
import static com.atlassian.upm.pageobjects.Waits.elementIsVisible;
import static com.atlassian.upm.pageobjects.Waits.loading;
import static com.atlassian.upm.pageobjects.Waits.not;
import static com.atlassian.upm.pageobjects.Waits.selectedAndLoaded;
import static com.atlassian.upm.pageobjects.WebElements.findPluginElement;
import static com.google.common.base.Suppliers.ofInstance;

public class ManageExistingTab extends UPMTab
{
    private static final String USER_PLUGINS_ID = "upm-user-plugins";
    private static final By USER_PLUGINS = By.id(USER_PLUGINS_ID);

    private static final String SYSTEM_PLUGINS_ID = "upm-system-plugins";
    private static final By SYSTEM_PLUGINS = By.id(SYSTEM_PLUGINS_ID);

    private static final String SHOW_SYSTEM_LINK_ID = "upm-manage-show-system";
    private static final By SHOW_SYSTEM_LINK = By.id(SHOW_SYSTEM_LINK_ID);

    private static final String HIDE_SYSTEM_LINK_ID = "upm-manage-hide-system";
    private static final By HIDE_SYSTEM_LINK = By.id(HIDE_SYSTEM_LINK_ID);

    private static final String REQUIRES_REFRESH_MESSAGE_ID = "upm-refresh-dialog";
    private static final By REQUIRES_REFRESH_MESSAGE = By.id(REQUIRES_REFRESH_MESSAGE_ID);

    private static final String ENABLE_SAFE_MODE_ID = "upm-safe-mode-enable";
    private static final By ENABLE_SAFE_MODE_LINK = By.id(ENABLE_SAFE_MODE_ID);
    
    private @Inject AtlassianWebDriver driver;
    private @Inject PageBinder binder;

    @FindBy(id="upm-panel-manage")
    private WebElement manageExistingPanel;

    @FindBy(id=SHOW_SYSTEM_LINK_ID)
    private WebElement showSystemPluginsLink;
    
    @WaitUntil
    public void waitUntilTabIsLoaded()
    {
        driver.waitUntil(selectedAndLoaded(manageExistingPanel));
    }

    @WaitUntil
    public void waitUntilUserPluginsSectionIsLoaded()
    {
        driver.waitUntil(and(elementIsVisible(USER_PLUGINS, ofInstance(manageExistingPanel)),
                             not(loading(findElement(USER_PLUGINS, manageExistingPanel).get()))));
    }

    public ManageExistingTabSection getUserInstalledPlugins()
    {
        return binder.bind(UserInstalledPlugins.class);
    }

    public SystemPlugins showSystemPlugins()
    {
        if (!systemPluginsOpen())
        {
            showSystemPluginsLink.click();
            driver.waitUntilElementIsVisible(SYSTEM_PLUGINS);
        }
        return binder.bind(SystemPlugins.class);
    }

    public boolean systemPluginsOpen()
    {
        return driver.elementIsVisible(SYSTEM_PLUGINS);
    }

    public boolean hasShowSystemPluginsLink()
    {
        return driver.elementExists(SHOW_SYSTEM_LINK) && driver.elementIsVisible(SHOW_SYSTEM_LINK);
    }

    public boolean hasHideSystemPluginsLink()
    {
        return driver.elementExists(HIDE_SYSTEM_LINK) && driver.elementIsVisible(HIDE_SYSTEM_LINK);
    }

    public boolean hasRequiresRefreshMessage()
    {
        return driver.elementExists(REQUIRES_REFRESH_MESSAGE);
    }

    public WebElement getSafeModeLink()
    {
        return driver.findElement(ENABLE_SAFE_MODE_LINK);
    }
    
    public InstalledPlugin getPlugin(String key)
    {
        return binder.bind(InstalledPlugin.class, findPluginElement(key, "manage", ofInstance(manageExistingPanel)));
    }

    public void uninstallPlugin(String key)
    {
        getPlugin(key).uninstall();
    }
    
    @Override
    protected String getPluginContainerId(TabCategory category)
    {
        return "#upm-" + category.getOptionValue() +"-plugins";
    }
    
    public enum Category implements TabCategory
    {
        USER_SECTION("user", USER_PLUGINS),
        SYSTEM_SECTION("system", SYSTEM_PLUGINS);

        private final String optionValue;
        private final By containerSelector;

        private Category(String optionValue, By containerSelector)
        {
            this.optionValue = optionValue;
            this.containerSelector = containerSelector;
        }

        public String getOptionValue()
        {
            return optionValue;
        }

        public By getContainerSelector()
        {
            return containerSelector;
        }

    }
}