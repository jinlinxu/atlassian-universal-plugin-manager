package it.com.atlassian.upm.uitests;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.pageobjects.page.HomePage;
import com.atlassian.pageobjects.page.LoginPage;
import com.atlassian.pageobjects.page.WebSudoPage;
import com.atlassian.upm.pageobjects.InstallTab;
import com.atlassian.upm.pageobjects.InstalledPluginDetails;
import com.atlassian.upm.pageobjects.ManageExistingTab;
import com.atlassian.upm.pageobjects.PluginManager;
import com.atlassian.upm.pageobjects.PluginModule;
import com.atlassian.upm.pageobjects.UpgradeTab;
import com.atlassian.upm.pageobjects.UpgradeablePluginDetails;
import com.atlassian.upm.test.RestTester;
import com.atlassian.upm.rest.async.AsynchronousTask;
import com.atlassian.upm.rest.async.CancellableTaskStatus;
import com.atlassian.upm.test.UpmUiTestRunner;
import com.atlassian.webdriver.pageobjects.WebDriverTester;

import com.google.inject.Inject;

import org.codehaus.jettison.json.JSONException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.upm.pageobjects.InstallTab.Category.POPULAR;
import static com.atlassian.upm.test.TestPlugins.CANNOT_DISABLE_MODULE;
import static com.atlassian.upm.test.TestPlugins.INSTALLABLE;
import static com.atlassian.upm.test.TestPlugins.INSTALLABLE_REQUIRES_RESTART;
import static com.atlassian.upm.test.TestPlugins.INSTALLABLE_WITH_CONFIG;
import static com.atlassian.upm.test.TestPlugins.NOT_RELOADABLE;
import static com.atlassian.upm.test.TestPlugins.STATIC;
import static com.atlassian.upm.test.UpmTestGroups.FECRU;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(UpmUiTestRunner.class)
public class LongRunningTaskTest
{
    @Inject private static TestedProduct<WebDriverTester> product;
    @Inject protected static RestTester restTester;

    private static PluginManager upm;
    private static InstallTab installTab;
    private static UpgradeTab upgradeTab;
    private static ManageExistingTab manageTab;

    private static AsynchronousTask.Representation<CancellableTaskStatus> task = null;

    @Before
    public void setUp() throws JSONException
    {
        task = restTester.createCancellableTask();
    }

    @After
    public void tearDown() throws JSONException
    {
        if (task != null)
        {
            restTester.cancelCancellableTask(task);
        }

        product.visit(HomePage.class).getHeader().logout(LoginPage.class);
    }

    @Test
    public void assertThatAuthorOfLongRunningTaskIsPresentedWithSpinner() throws Exception
    {
        upm = product.visit(LoginPage.class).
            loginAsSysAdmin(WebSudoPage.class).
            confirm(PluginManager.class);
        reloadInstallTab();

        assertTrue(installTab.waitUntilProgressDialogShowing());

    }

    @Test
    @TestGroups(excludes = FECRU, reason = "Fisheye data doesn't have multiple admins.")
    public void assertThatPendingTaskMessageAppears() throws Exception
    {
        loginAsFred(); // because the long-running task was started by admin
        reloadInstallTab();

        assertThat(installTab.getPendingTasksText(), containsString("A long-running task has been initiated by another user."));
    }

    @Test
    @TestGroups(excludes = FECRU, reason = "Fisheye data doesn't have multiple admins.")
    public void assertThatInstallPluginActionNotAvailableForNonTaskAuthor() throws Exception
    {
        loginAsFred(); // because the long-running task was started by admin
        reloadInstallTab();

        // make sure fred can't install plugins now
        assertFalse(installTab.show(POPULAR)
                       .getPlugin(INSTALLABLE.getKey())
                       .openPluginDetails()
                       .getInstallButton()
                       .isDisplayed());
    }

    @Test
    @TestGroups(excludes = FECRU, reason = "Fisheye data doesn't have multiple admins.")
    public void assertThatUploadPluginActionNotAvailableForNonTaskAuthor() throws Exception
    {
        loginAsFred(); // because the long-running task was started by admin
        reloadInstallTab();

        // make sure fred can't upload plugins now
        installTab.waitForUploadButtonToHide(); // not sure why I had to wait here since we already waited for the tab to load in reloadInstallTab() just above, but otherwise sometimes the button isn't hidden yet
        assertFalse(installTab.getUploadButton().isDisplayed());
    }

    @Test
    @TestGroups(excludes = FECRU, reason = "Fisheye data doesn't have multiple admins.")
    public void assertThatCancelRequiresRestartActionNotAvailableForNonTaskAuthor() throws Exception
    {
        try
        {
            restTester.installPluginAndWaitForCompletion(INSTALLABLE_REQUIRES_RESTART);
            loginAsFred(); // because the long-running task was started by admin
            reloadInstallTab();

            // make sure fred can't cancel install of plugins that require restart now
            assertFalse(installTab.isElementShowing("upm-cancel-requires-restart-"+INSTALLABLE_REQUIRES_RESTART.getKey()));
        }
        finally
        {
            restTester.uninstallPlugin(INSTALLABLE_REQUIRES_RESTART);
        }

    }

    @Test
    @TestGroups(excludes = FECRU, reason = "Fisheye data doesn't have multiple admins.")
    public void assertThatUpgradeAllActionNotAvailableForNonTaskAuthor() throws Exception
    {
        loginAsFred(); // because the long-running task was started by admin
        reloadUpgradeTab();

        // make sure fred can't upgrade all plugins now
        assertFalse(upgradeTab.getUpgradeAllButton().isDisplayed());
    }

    @Test
    @TestGroups(excludes = FECRU, reason = "Fisheye data doesn't have multiple admins.")
    public void assertThatUpgradePluginActionNotAvailableForNonTaskAuthor() throws Exception
    {
        loginAsFred(); // because the long-running task was started by admin
        reloadUpgradeTab();

        // make sure fred can't upgrade a plugin now
        UpgradeablePluginDetails upgradeablePluginDetails = upgradeTab.getPlugin(CANNOT_DISABLE_MODULE.getKey()).openPluginDetails();
        upgradeablePluginDetails.waitUntilPluginDetailsAreLoaded();
        assertFalse(upgradeablePluginDetails.getUpgradeButton().isDisplayed());
    }

    @Test
    @TestGroups(excludes = FECRU, reason = "Fisheye data doesn't have multiple admins.")
    public void assertThatEnableSafeModeActionNotAvailableForNonTaskAuthor() throws Exception
    {
        loginAsFred(); // because the long-running task was started by admin
        reloadManageTab();

        // make sure fred can't enable safe mode now
        assertFalse(manageTab.getSafeModeLink().isDisplayed());
    }

    @Test
    @TestGroups(excludes = FECRU, reason = "Fisheye data doesn't have multiple admins.")
    public void assertThatUninstallPluginActionNotAvailableForNonTaskAuthor() throws Exception
    {
        try
        {
            restTester.installPluginAndWaitForCompletion(INSTALLABLE_WITH_CONFIG);
            loginAsFred(); // because the long-running task was started by admin
            reloadManageTab();

            // make sure fred can't uninstall a plugin now
            InstalledPluginDetails installedPluginDetails = manageTab.getPlugin(INSTALLABLE_WITH_CONFIG.getKey()).openPluginDetails();
            assertFalse(installedPluginDetails.canBeUninstalled());
        }
        finally
        {
            restTester.uninstallPlugin(INSTALLABLE_WITH_CONFIG);
        }
    }

    @Test
    @TestGroups(excludes = FECRU, reason = "Fisheye data doesn't have multiple admins.")
    public void assertThatDisablePluginActionNotAvailableForNonTaskAuthor() throws Exception
    {
        loginAsFred(); // because the long-running task was started by admin
        reloadManageTab();

        // make sure fred can't disable a plugin now
        InstalledPluginDetails installedPluginDetails = manageTab.getPlugin(STATIC.getKey()).openPluginDetails();
        assertFalse(installedPluginDetails.canBeDisabled());
    }

    @Test
    @TestGroups(excludes = FECRU, reason = "Fisheye data doesn't have multiple admins.")
    public void assertThatDisablePluginModuleActionNotAvailableForNonTaskAuthor() throws Exception
    {
        loginAsFred(); // because the long-running task was started by admin
        reloadManageTab();

        // make sure fred can't disable a module now
        InstalledPluginDetails installedPluginDetails = manageTab.showSystemPlugins().getPlugin(NOT_RELOADABLE.getKey()).openPluginDetails();
        PluginModule pluginModule = installedPluginDetails.openModuleManager().getModule("notreloadable");
        assertFalse(pluginModule.isDisableButtonVisible());
    }

    private void loginAsFred()
    {
        upm = product.visit(LoginPage.class)
             .login("fred", "fred", WebSudoPage.class)
             .confirm("fred", PluginManager.class);
    }

    private static void reloadInstallTab()
    {
        upm = product.visit(PluginManager.class);
        installTab = upm.openInstallTab();
        installTab.waitUntilTabIsLoaded();
    }

    private static void reloadUpgradeTab()
    {
        upm = product.visit(PluginManager.class);
        upgradeTab = upm.openUpgradeTab();
        upgradeTab.waitUntilTabIsLoaded();
    }

    private static void reloadManageTab()
    {
        upm = product.visit(PluginManager.class);
        manageTab = upm.openManageExistingTab();
        manageTab.waitUntilTabIsLoaded();
    }
}
