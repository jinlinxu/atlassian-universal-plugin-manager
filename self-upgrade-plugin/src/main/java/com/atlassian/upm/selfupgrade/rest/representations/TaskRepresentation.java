package com.atlassian.upm.selfupgrade.rest.representations;

import java.net.URI;
import java.util.Date;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

public class TaskRepresentation
{
    @JsonProperty private final String type;
    @JsonProperty private final Integer pingAfter;
    @JsonProperty private final TaskStatusRepresentation status;
    @JsonProperty private final Map<String, URI> links;
    @JsonProperty private final String username;
    @JsonProperty private final Date timestamp;

    @JsonCreator
    public TaskRepresentation(@JsonProperty("type") String type,
        @JsonProperty("pingAfter") Integer pingAfter,
        @JsonProperty("status") TaskStatusRepresentation status,
        @JsonProperty("links") Map<String, URI> links,
        @JsonProperty("timestamp") Date timestamp,
        @JsonProperty("username") String username)
    {
        this.type = type;
        this.pingAfter = pingAfter;
        this.status = status;
        this.links = links;
        this.timestamp = timestamp;
        this.username = username;
    }

    public Integer getPingAfter()
    {
        return pingAfter;
    }

    public String getType()
    {
        return type;
    }

    public TaskStatusRepresentation getStatus()
    {
        return status;
    }

    public String getUsername()
    {
        return username;
    }

    public Date getTimestamp()
    {
        return timestamp;
    }

    public String getContentType()
    {
        return status.getContentType();
    }

    public URI getSelf()
    {
        return links.get("self");
    }
}
