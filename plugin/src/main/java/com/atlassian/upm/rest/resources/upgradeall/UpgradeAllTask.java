package com.atlassian.upm.rest.resources.upgradeall;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

import com.atlassian.plugins.PacException;
import com.atlassian.plugins.domain.model.plugin.PluginVersion;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.upm.AccessDeniedException;
import com.atlassian.upm.Either;
import com.atlassian.upm.LegacyPluginsUnsupportedException;
import com.atlassian.upm.PluginAccessorAndController;
import com.atlassian.upm.PluginDownloadService;
import com.atlassian.upm.PluginDownloadService.Progress;
import com.atlassian.upm.PluginDownloadService.ProgressTracker;
import com.atlassian.upm.PluginInstaller;
import com.atlassian.upm.RelativeURIException;
import com.atlassian.upm.SafeModeException;
import com.atlassian.upm.UnknownPluginTypeException;
import com.atlassian.upm.UnrecognisedPluginVersionException;
import com.atlassian.upm.UnsupportedProtocolException;
import com.atlassian.upm.XmlPluginsUnsupportedException;
import com.atlassian.upm.log.AuditLogService;
import com.atlassian.upm.pac.PacClient;
import com.atlassian.upm.rest.UpmUriBuilder;
import com.atlassian.upm.rest.async.AsynchronousTask;
import com.atlassian.upm.rest.representations.PluginRepresentation;

import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.upm.rest.resources.upgradeall.UpgradeStatus.err;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.ImmutableMap.of;
import static com.google.common.collect.Iterables.filter;

public final class UpgradeAllTask extends AsynchronousTask<UpgradeStatus>
{
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final PacClient pacClient;
    private final PluginAccessorAndController pluginAccessorAndController;
    private final PluginInstaller pluginInstaller;
    private final PluginDownloadService pluginDownloadService;
    private final AuditLogService auditLogger;
    private final UpmUriBuilder uriBuilder;

    public UpgradeAllTask(PacClient pacClient,
        PluginDownloadService pluginDownloadService,
        PluginAccessorAndController pluginAccessorAndController,
        PluginInstaller pluginInstaller,
        AuditLogService auditLogger,
        UpmUriBuilder uriBuilder,
        String username)
    {
        super(Type.UPGRADE_ALL, username);
        this.pluginDownloadService = pluginDownloadService;
        this.pluginAccessorAndController = pluginAccessorAndController;
        this.pluginInstaller = pluginInstaller;
        this.pacClient = pacClient;
        this.auditLogger = auditLogger;
        this.uriBuilder = uriBuilder;
    }

    public void accept()
    {
        status = findingUpgrades();
    }

    public URI call()
    {
        try
        {
            status = upgrade(download(findUpgrades()));
        }
        catch (PacException pe)
        {
            status = err("err.finding.upgrades");
            logger.error("Failed to find available upgrades: " + pe);
        }
        catch (Throwable t)
        {
            status = err("unexpected.exception");
            logger.error("Failed to upgrade all plugins", t);
        }
        return null;
    }

    UpgradeAllResults getResults()
    {
        if (status.isDone())
        {
            return (UpgradeAllResults) status;
        }
        return null;
    }

    static final UpgradeStatus FINDING_UPGRADES = new UpgradeStatus(UpgradeStatus.State.FINDING_UPGRADES);

    static UpgradeStatus findingUpgrades()
    {
        return FINDING_UPGRADES;
    }

    private Iterable<PluginVersion> findUpgrades()
    {
        return filter(pacClient.getUpgrades(), not(isUpmPluginVersion));
    }
    
    private Predicate<PluginVersion> isUpmPluginVersion = new Predicate<PluginVersion>()
    {
        public boolean apply(PluginVersion pv)
        {
            return pluginAccessorAndController.getUpmPluginKey().equals(pv.getPlugin().getPluginKey());
        }
    };

    static UpgradeStatus downloading(final PluginVersion pluginVersion, int numberComplete, int totalUpgrades) throws URISyntaxException
    {
        return new DownloadingPluginStatus(pluginVersion, numberComplete, totalUpgrades);
    }

    static UpgradeStatus downloading(final PluginVersion pluginVersion, Progress progress) throws URISyntaxException
    {
        return new DownloadingPluginStatus(pluginVersion, progress);
    }

    static UpgradeStatus downloading(final PluginVersion pluginVersion, URI redirectedUri)
    {
        return new DownloadingPluginStatus(pluginVersion, redirectedUri);
    }

    private Map<PluginVersion, Either<File, UpgradeFailed>> download(Iterable<PluginVersion> upgrades)
    {
        ImmutableMap.Builder<PluginVersion, Either<File, UpgradeFailed>> upgradeFiles = ImmutableMap.builder();
        for (PluginVersion upgrade : upgrades)
        {
            Either<File, UpgradeFailed> result;
            try
            {
                if (upgrade.getPlugin().getDeployable())
                {
                    int numberComplete = upgradeFiles.build().size();
                    int totalUpgrades = Iterables.size(upgrades);
                    status = downloading(upgrade, numberComplete, totalUpgrades);
                    result = Either.left(
                        pluginDownloadService.downloadPlugin(new URI(upgrade.getBinaryUrl().trim()), null, null, newProgressTracker(upgrade)));
                }
                else
                {
                    result = Either.right(new UpgradeFailed(UpgradeFailed.Type.INSTALL, "not.deployable", upgrade));
                    auditLogger.logI18nMessage("upm.auditLog.upgrade.plugin.failure", upgrade.getBinaryUrl());
                }
            }
            catch (AccessDeniedException e)
            {
                result = Either.right(new UpgradeFailed(UpgradeFailed.Type.DOWNLOAD, "access.denied", upgrade));
                logger.error("Access denied when downloading " + upgrade.getBinaryUrl(), e);
                auditLogger.logI18nMessage("upm.auditLog.upgrade.plugin.failure", upgrade.getBinaryUrl());
            }
            catch (UnsupportedProtocolException e)
            {
                result = Either.right(new UpgradeFailed(UpgradeFailed.Type.DOWNLOAD, "unsupported.protocol", upgrade, e.getMessage()));
                logger.error("Failed to download plugin " + upgrade.getBinaryUrl(), e);
                auditLogger.logI18nMessage("upm.auditLog.upgrade.plugin.failure", upgrade.getBinaryUrl());
            }
            catch (RelativeURIException e)
            {
                result = Either.right(new UpgradeFailed(UpgradeFailed.Type.DOWNLOAD, "invalid.relative.uri", upgrade, e.getMessage()));
                logger.error("Failed to download plugin " + upgrade.getBinaryUrl(), e);
                auditLogger.logI18nMessage("upm.auditLog.upgrade.plugin.failure", upgrade.getBinaryUrl());
            }
            catch (ResponseException e)
            {
                result = Either.right(new UpgradeFailed(UpgradeFailed.Type.DOWNLOAD, "response.exception", upgrade, e.getMessage()));
                logger.error("Failed to download " + upgrade.getBinaryUrl(), e);
                auditLogger.logI18nMessage("upm.auditLog.upgrade.plugin.failure", upgrade.getBinaryUrl());
            }
            catch (URISyntaxException e)
            {
                result = Either.right(new UpgradeFailed(UpgradeFailed.Type.DOWNLOAD, "invalid.uri.syntax", upgrade, e.getMessage()));
                logger.error("Invalid plugin binary URL " + upgrade.getBinaryUrl(), e);
                auditLogger.logI18nMessage("upm.auditLog.upgrade.plugin.failure", upgrade.getBinaryUrl());
            }
            upgradeFiles.put(upgrade, result);
        }
        return upgradeFiles.build();
    }

    private ProgressTracker newProgressTracker(final PluginVersion pluginVersion)
    {
        return new ProgressTracker()
        {
            public void notify(Progress progress)
            {
                try
                {
                    status = downloading(pluginVersion, progress);
                }
                catch (URISyntaxException e)
                {
                    // Won't ever happen.  If the URI was bad, it would have failed long before now.  
                }
            }

            public void redirectedTo(URI newUri)
            {
                status = downloading(pluginVersion, newUri);
            }
        };
    }

    static UpgradeStatus upgrading(final PluginVersion pluginVersion, int numberComplete, int totalUpgrades)
    {
        return new UpgradingPluginStatus(pluginVersion, numberComplete, totalUpgrades);
    }

    private UpgradeStatus upgrade(Map<PluginVersion, Either<File, UpgradeFailed>> upgrades)
    {
        boolean upgradeRequiresRestart = false;
        final ImmutableList.Builder<UpgradeSucceeded> successes = ImmutableList.builder();
        final ImmutableList.Builder<UpgradeFailed> failures = ImmutableList.builder();
        for (Map.Entry<PluginVersion, Either<File, UpgradeFailed>> upgrade : upgrades.entrySet())
        {
            if (upgrade.getValue().isLeft())
            {
                try
                {
                    int numberComplete = successes.build().size() + failures.build().size();
                    int totalUpgrades = upgrades.size();
                    status = upgrading(upgrade.getKey(), numberComplete, totalUpgrades);
                    final PluginRepresentation pluginRepresentation = pluginInstaller.upgrade(upgrade.getValue().left(), upgrade.getKey().getPlugin().getName());
                    if (pluginRepresentation.getChangeRequiringRestartLink() != null)
                    {
                        upgradeRequiresRestart = true;
                    }
                    successes.add(new UpgradeSucceeded(upgrade.getKey()));
                }
                catch (LegacyPluginsUnsupportedException lpue)
                {
                    failures.add(new UpgradeFailed(UpgradeFailed.Type.INSTALL, "legacy.plugins.unsupported", upgrade.getKey()));
                }
                catch (XmlPluginsUnsupportedException lpue)
                {
                    failures.add(new UpgradeFailed(UpgradeFailed.Type.INSTALL, "xml.plugins.unsupported", upgrade.getKey()));
                }
                catch (UnrecognisedPluginVersionException upve) 
                {
                    failures.add(new UpgradeFailed(UpgradeFailed.Type.INSTALL, "unrecognised.plugin.version", upgrade.getKey()));
                }
                catch (UnknownPluginTypeException upte)
                {
                    failures.add(new UpgradeFailed(UpgradeFailed.Type.INSTALL, "unknown.plugin.type", upgrade.getKey()));
                }
                catch (SafeModeException sme)
                {
                    failures.add(new UpgradeFailed(UpgradeFailed.Type.INSTALL, "safe.mode", upgrade.getKey()));
                }
                catch (RuntimeException re)
                {
                    failures.add(new UpgradeFailed(UpgradeFailed.Type.INSTALL, "install.failed", upgrade.getKey(), re.getMessage()));
                }
            }
            else
            {
                failures.add(upgrade.getValue().right());
            }
        }

        // UPM-884 - need to get UPM to check for requires restart for the upgraded plugins
        final Map<String, URI> links;
        if (upgradeRequiresRestart)
        {
            links = of("changes-requiring-restart", uriBuilder.buildChangesRequiringRestartUri());
        }
        else
        {
            links = of();
        }

        return new UpgradeAllResults(successes.build(), failures.build(), links);
    }
}
