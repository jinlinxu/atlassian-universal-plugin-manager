package com.atlassian.upm.rest.representations;

import java.net.URI;
import java.util.Collection;
import java.util.Map;

import com.atlassian.plugins.domain.model.plugin.PluginVersion;
import com.atlassian.upm.PluginAccessorAndController;
import com.atlassian.upm.rest.UpmUriBuilder;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.transform;

/**
 * Collection of plugins that can be installed in the application.
 */
public abstract class AbstractInstallablePluginCollectionRepresentation
{
    @JsonProperty private final Map<String, URI> links;
    @JsonProperty private final Collection<AvailablePluginEntry> plugins;
    @JsonProperty private final boolean safeMode;

    @JsonCreator
    public AbstractInstallablePluginCollectionRepresentation(
        @JsonProperty("links") Map<String, URI> links,
        @JsonProperty("plugins") Collection<AvailablePluginEntry> plugins,
        @JsonProperty("safeMode") boolean safeMode)
    {
        this.plugins = ImmutableList.copyOf(plugins);
        this.links = ImmutableMap.copyOf(links);
        this.safeMode = safeMode;
    }

    public AbstractInstallablePluginCollectionRepresentation(Map<String, URI> links,
        Iterable<PluginVersion> plugins, UpmUriBuilder uriBuilder,
        PluginAccessorAndController pluginAccessorAndController)
    {
        this.links = links;
        this.plugins = ImmutableList.copyOf(transform(plugins, toEntries(uriBuilder)));
        this.safeMode = pluginAccessorAndController.isSafeMode();
    }

    public Map<String, URI> getLinks()
    {
        return links;
    }

    public Iterable<AvailablePluginEntry> getPlugins()
    {
        return plugins;
    }

    public boolean isSafeMode()
    {
        return safeMode;
    }

    private Function<PluginVersion, AvailablePluginEntry> toEntries(UpmUriBuilder uriBuilder)
    {
        return new ToEntryFunction(uriBuilder);
    }

    private final static class ToEntryFunction implements Function<PluginVersion, AvailablePluginEntry>
    {
        private final UpmUriBuilder uriBuilder;

        public ToEntryFunction(UpmUriBuilder uriBuilder)
        {
            this.uriBuilder = uriBuilder;
        }

        public AvailablePluginEntry apply(PluginVersion plugin)
        {
            return new AvailablePluginEntry(plugin, uriBuilder);
        }
    }

    public static final class AvailablePluginEntry
    {
        @JsonProperty private final Map<String, URI> links;
        @JsonProperty private final String name;
        @JsonProperty private final String key;
        @JsonProperty private final String summary;

        @JsonCreator
        public AvailablePluginEntry(@JsonProperty("links") Map<String, URI> links,
            @JsonProperty("name") String name,
            @JsonProperty("summary") String summary,
            @JsonProperty("key") String key)
        {
            this.links = ImmutableMap.copyOf(links);
            this.name = checkNotNull(name, "name");
            this.key = checkNotNull(key, "key");
            this.summary = checkNotNull(summary, "summary");
        }

        AvailablePluginEntry(PluginVersion plugin, UpmUriBuilder uriBuilder)
        {
            this.links = ImmutableMap.of("self", uriBuilder.buildAvailablePluginUri(plugin.getPlugin().getPluginKey()));
            this.name = plugin.getPlugin().getName();
            this.key = plugin.getPlugin().getPluginKey();
            this.summary = plugin.getSummary();
        }

        public URI getSelf()
        {
            return links.get("self");
        }

        public String getName()
        {
            return name;
        }

        public String getKey()
        {
            return key;
        }

        public String getSummary()
        {
            return summary;
        }

        @Override
        public String toString()
        {
            return name;
        }
    }
}
