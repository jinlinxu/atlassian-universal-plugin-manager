package com.atlassian.upm.rest.resources.upgradeall;

import com.atlassian.upm.rest.async.TaskStatus;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import static com.atlassian.upm.rest.MediaTypes.UPGRADE_ALL_COMPLETE_JSON;
import static com.atlassian.upm.rest.MediaTypes.UPGRADE_ALL_DOWNLOADING_JSON;
import static com.atlassian.upm.rest.MediaTypes.UPGRADE_ALL_ERR_JSON;
import static com.atlassian.upm.rest.MediaTypes.UPGRADE_ALL_FINDING_JSON;
import static com.atlassian.upm.rest.MediaTypes.UPGRADE_ALL_UPGRADING_JSON;

public class UpgradeStatus extends TaskStatus
{
    public UpgradeStatus(UpgradeStatus.State state)
    {
        super(state.isDone(), state.getContentType());
    }

    public enum State
    {
        FINDING_UPGRADES(UPGRADE_ALL_FINDING_JSON),
        DOWNLOADING(UPGRADE_ALL_DOWNLOADING_JSON),
        UPGRADING(UPGRADE_ALL_UPGRADING_JSON),
        COMPLETE(UPGRADE_ALL_COMPLETE_JSON),
        ERR(UPGRADE_ALL_ERR_JSON);

        private final String contentType;

        private State(String contentType)
        {
            this.contentType = contentType;
        }

        public boolean isDone()
        {
            return this == COMPLETE;
        }

        public String getContentType()
        {
            return contentType;
        }
    }

    public static Err err(String subCode)
    {
        return new Err(subCode);
    }

    public static class Err extends UpgradeStatus
    {
        @JsonProperty private final String subCode;

        @JsonCreator
        public Err(@JsonProperty("subCode") String subCode)
        {
            super(State.ERR);
            this.subCode = subCode;
        }

        public String getSubCode()
        {
            return subCode;
        }
    }
}