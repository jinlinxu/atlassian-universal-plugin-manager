package com.atlassian.upm.pac;

import com.atlassian.plugins.domain.model.plugin.PluginVersion;
import com.atlassian.plugins.domain.model.product.Product;
import com.atlassian.upm.ProductUpgradePluginCompatibility;

import static com.atlassian.upm.Options.Option;

/**
 * Provides ability to check http://plugins.atlassian.com for available plugins to install and upgrade for the
 * application and version that the plugin manager is currently in.
 */
public interface PacClient
{
    /**
     * Fetches the plugins that are available to install and filters out any that are already installed.
     *
     * @param query specifies the query to be executed
     * @param max The maximum number of results to return, null if all results should be returned
     * @param offset The offset of the results to return, null to start at the beginning of the list
     * @return plugins that are available to install
     */
    Iterable<PluginVersion> getAvailable(String query, Integer max, Integer offset);

    /**
     * Fetches the plugin details for the specified plugin key or {@code null} if no plugin with that key exists.
     *
     * @param key key of the plugin to fetch the details of
     * @return plugin details for the specified plugin key or {@code null} if no plugin with that key exists
     */
    PluginVersion getAvailablePlugin(String key);

    /**
     * Fetches the plugin details for the specified plugin key and version or {@code null} if no plugin with that key
     * and version exists.
     *
     * @param key key of the plugin to fetch the details of
     * @param version version of the plugin to fetch the details of
     * @return plugin details for the specified plugin key and version or {@code null} if no plugin with that key and
     *         version exists
     */
    PluginVersion getPluginVersion(String key, String version);

    /**
     * Fetches the popular plugins that are available to install and filters out any that are already installed.
     *
     * @param max The maximum number of results to return, null if all results should be returned
     * @param offset The offset of the results to return, null to start at the beginning of the list
     * @return plugins that are popular and available to install
     */
    Iterable<PluginVersion> getPopular(Integer max, Integer offset);

    /**
     * Fetches the supported plugins that are available to install and filters out any that are already installed.
     *
     * @param max The maximum number of results to return, null if all results should be returned
     * @param offset The offset of the results to return, null to start at the beginning of the list
     * @return plugins that are supported and available to install
     */
    Iterable<PluginVersion> getSupported(Integer max, Integer offset);

    /**
     * Fetches the featured plugins that are available to install and filters out any that are already installed.
     *
     * @param max The maximum number of results to return, null if all results should be returned
     * @param offset The offset of the results to return, null to start at the beginning of the list
     * @return plugins that are available to install
     */
    Iterable<PluginVersion> getFeatured(Integer max, Integer offset);

    /**
     * @return the product versions released after the current version
     */
    Iterable<Product> getProductUpgrades();

    /**
     * Looks for updates for installed plugins that are compatible with the installed application.
     *
     * @return plugins that can be upgraded
     */
    Iterable<PluginVersion> getUpgrades();

    /**
     * Horrible hack to work around Jersey shenanigans when called from PluginUpgradeCheckScheduler.
     *
     * @return number of plugins that can be upgraded
     */
    int getUpgradeCount();

    /**
     * Determines and returns the list of compatibility statuses of the currently installed plugins against the
     * specified product upgrade version. Any installed plugins that are not found on PAC go in the unknown status.
     *
     * @param upgradeBuildNumber specifies the product version to check compatibility of plugins against
     * @return the compatibility statuses of the currently installed plugins
     */
    ProductUpgradePluginCompatibility getProductUpgradePluginCompatibility(Long upgradeBuildNumber);

    /**
     * Returns true if PAC does not know about the currently running Product version, false otherwise. Note that PAC
     * "knows" product versions not on its lists if they are in between known versions. The result is cached; the first
     * call may access PAC, but subsequent calls will not, since the value cannot change while the product is running.
     *
     * @return true if unknown, false if known, or none if either PAC was unreachable, or the product version couldn't be computed
     * @since 1.3.2
     */
    Option<Boolean> isUnknownProductVersion();

    /**
     * Returns true if the currently running Product version is a development version, false otherwise. The result is cached;
     * the first call may access PAC, but subsequent calls will not, since the value cannot change while the product is running.
     * @return true if a development version, false if it is not a development version, or none if the product version couldn't be computed
     * @since 1.6
     */
    Option<Boolean> isDevelopmentProductVersion();

    /**
     * Returns true if PAC is disabled. UPM can be set offline by setting the "upm.pac.disable" System Property to true
     * which is overridden by the PluginSettings property
     *
     * @return true if PAC is disabled; false if methods actually send a request to PAC each time they are called.
     */
    boolean isPacDisabled();

    /**
     * Returns true if the PAC server is reachable; false otherwise
     * @return true if the PAC server is reachable; false otherwise
     * @since 1.6
     */
    boolean isPacReachable();

}
