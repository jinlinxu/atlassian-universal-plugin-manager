package com.atlassian.upm;

import static com.google.common.base.Preconditions.checkNotNull;

public final class Options
{
    public interface Option<T>
    {
        T get();
        T getOrElse(T orElse);
        boolean isDefined();
    }

    public static final class Some<T> implements Option<T>
    {
        private final T obj;

        private Some(T obj)
        {
            this.obj = checkNotNull(obj, "obj");
        }

        public T get()
        {
            return obj;
        }
        
        public T getOrElse(T orElse)
        {
            return obj;
        }

        public boolean isDefined()
        {
            return true;
        }
    }

    public static final class None<T> implements Option<T>
    {
        private None() {}

        public T get()
        {
            return null;
        }
        
        public T getOrElse(T orElse)
        {
            return orElse;
        }

        public boolean isDefined()
        {
            return false;
        }
    }

    public static <T> Option<T> option(T obj)
    {
        return obj == null ? new None<T>() : new Some<T>(obj);
    }
    
    public static <T> Option<T> none()
    {
        return new None<T>();
    }
    
    public static <T> Option<T> none(Class<T> noneClass)
    {
        return new None<T>();
    }
}
