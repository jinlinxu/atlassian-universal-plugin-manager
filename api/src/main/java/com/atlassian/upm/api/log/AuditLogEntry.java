package com.atlassian.upm.api.log;

import java.util.Date;

import com.atlassian.sal.api.message.I18nResolver;

/**
 * Represents a single entry in the UPM's Audit Log. Entries can represent any kind of plugin event:
 * plugin installation, plugin uninstallation, plugin enablement, and more.
 *
 * @since 1.6
 */
public interface AuditLogEntry extends Comparable<AuditLogEntry>
{
    /**
     * Returns the log entry's title.
     * @param i18nResolver the I18nResolver used to translate the title
     * @return the log entry's title.
     */
    String getTitle(I18nResolver i18nResolver);

    /**
     * Returns the log entry's message content
     * @param i18nResolver the I18nResolver used to translate the title
     * @return the log entry's message content
     */
    String getMessage(I18nResolver i18nResolver);

    /**
     * Returns the date at which this log entry was created
     * @return the date at which this log entry was created
     */
    Date getDate();

    /**
     * Returns the username of the user that was logged in and created this event
     * @return the username of the user that was logged in and created this event
     */
    String getUsername();

    /**
     * Returns the i18n key of the event message
     * @return the i18n key of the event message
     */
    String getI18nKey();

    /**
     * Returns the entry type of the event
     * @return the entry type of the event
     * @since 1.6
     */
    EntryType getEntryType();
}
