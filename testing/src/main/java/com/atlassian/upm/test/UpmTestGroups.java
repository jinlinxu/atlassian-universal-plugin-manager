package com.atlassian.upm.test;

public final class UpmTestGroups
{
    public static final String BAMBOO = "bamboo";
    public static final String CONFLUENCE = "confluence";
    public static final String FECRU = "fecru";
    public static final String JIRA = "jira";
    public static final String REFAPP = "refapp";
}
