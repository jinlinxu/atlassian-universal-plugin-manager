package com.atlassian.upm.test;

import com.atlassian.integrationtesting.runner.TestGroupRunner;
import com.atlassian.upm.rest.UpmUriBuilder;
import com.atlassian.upm.rest.representations.ErrorRepresentation;

import com.google.inject.Inject;
import com.sun.jersey.api.client.ClientResponse;

import org.junit.After;
import org.junit.runner.RunWith;

@RunWith(TestGroupRunner.class)
public class UpmResourceTestBase
{
    @Inject protected final RestTester restTester;

    public UpmResourceTestBase()
    {
        this.restTester = new RestTester();
    }

    @After
    public final void destroyTester()
    {
        restTester.destroy();
    }

    protected ErrorRepresentation getErrorRepresentationFromResponse(ClientResponse response)
    {
        if (response != null)
        {
            return response.getEntity(ErrorRepresentation.class);
        }
        return null;
    }

    protected final void checkNotInstalled(TestPlugins testPlugin)
    {
        restTester.checkNotInstalled(testPlugin);
    }

    public UpmUriBuilder getUriBuilder()
    {
        return restTester.getUriBuilder();
    }
}
