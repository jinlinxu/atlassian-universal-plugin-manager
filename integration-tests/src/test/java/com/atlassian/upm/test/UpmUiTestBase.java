package com.atlassian.upm.test;

import com.google.inject.Inject;

import org.junit.AfterClass;
import org.junit.runner.RunWith;

@RunWith(UpmTestRunner.class)
public class UpmUiTestBase
{
    @Inject protected static UpmUiTester uiTester;
    @Inject protected static RestTester restTester = new RestTester();

    @AfterClass
    public static void destroyTester()
    {
        restTester.destroy();
    }
}
