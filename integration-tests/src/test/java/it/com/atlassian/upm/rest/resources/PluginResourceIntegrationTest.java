package it.com.atlassian.upm.rest.resources;

import java.net.URISyntaxException;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.upm.rest.representations.ErrorRepresentation;
import com.atlassian.upm.rest.representations.PluginRepresentation;
import com.atlassian.upm.test.RestTester;
import com.atlassian.upm.test.UpmResourceTestBase;

import com.sun.jersey.api.client.ClientResponse;

import org.codehaus.jettison.json.JSONException;
import org.junit.Ignore;
import org.junit.Test;

import static com.atlassian.upm.test.UpmTestGroups.CONFLUENCE;

import static com.atlassian.upm.test.JerseyClientMatchers.accepted;
import static com.atlassian.upm.test.TestPlugins.BUNDLED;
import static com.atlassian.upm.test.TestPlugins.BUNDLED_WITH_CONFIG;
import static com.atlassian.upm.test.TestPlugins.INSTALLABLE;
import static com.atlassian.upm.test.TestPlugins.INSTALLABLE_REQUIRES_RESTART;
import static com.atlassian.upm.test.TestPlugins.INSTALLABLE_WITH_CONFIG;
import static com.atlassian.upm.test.TestPlugins.STATIC;
import static com.atlassian.upm.test.TestPlugins.SYSTEM;
import static com.atlassian.upm.test.TestPlugins.UPGRADABLE_BUNDLED;
import static com.atlassian.upm.test.TestPlugins.UPGRADEABLE_REQUIRES_RESTART_V1;
import static com.atlassian.upm.test.TestPlugins.UPGRADEABLE_REQUIRES_RESTART_V2;
import static com.atlassian.upm.test.UpmTestGroups.FECRU;
import static com.atlassian.upm.test.UpmTestGroups.REFAPP;
import static com.google.common.base.Preconditions.checkState;
import static javax.ws.rs.core.Response.Status.CONFLICT;
import static javax.ws.rs.core.Response.Status.FORBIDDEN;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.Status.OK;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PluginResourceIntegrationTest extends UpmResourceTestBase
{
    private static final String TEST_NONEXISTENT_PLUGIN_KEY = "nonexistent-pluginkey";

    @Test
    public void enablePluginWithInvalidPluginKeyReturnsNotFound() throws URISyntaxException
    {
        ClientResponse response = restTester.enablePlugin(TEST_NONEXISTENT_PLUGIN_KEY);
        assertThat(response.getResponseStatus(), is(NOT_FOUND));
    }

    @Test
    public void disablePluginWithInvalidPluginKeyReturnsNotFound() throws URISyntaxException
    {
        ClientResponse response = restTester.disablePlugin(TEST_NONEXISTENT_PLUGIN_KEY);
        assertThat(response.getResponseStatus(), is(NOT_FOUND));
    }

    @Test
    public void enablePluginWithValidKeyReturnsOK() throws URISyntaxException
    {
        // make sure that the test plugin is disabled, so we can test enabling
        restTester.disablePlugin(BUNDLED);

        ClientResponse response = restTester.enablePlugin(BUNDLED);
        assertThat(response.getResponseStatus(), is(OK));
        PluginRepresentation returnedRepresentation = response.getEntity(PluginRepresentation.class);
        assertThat(returnedRepresentation.isEnabled(), is(true));
    }

    @Test
    @TestGroups(value = REFAPP, reason = "Only refapp has sufficient test data to support this (UPM-821, UPM-820)")
    public void checkThatNonAdminCannotAccessPluginResource()
    {
        RestTester nonAdminRestTester = new RestTester("barney", "barney");
        try
        {
            ClientResponse response = nonAdminRestTester.getPlugin(BUNDLED, ClientResponse.class);
            assertThat(response.getResponseStatus(), is(UNAUTHORIZED));
        }
        finally
        {
            nonAdminRestTester.destroy();
        }
    }

    @Test
    public void disablePluginWithValidKeyReturnsOK() throws URISyntaxException
    {
        try
        {
            ClientResponse response = restTester.disablePlugin(BUNDLED);
            assertThat(response.getResponseStatus(), is(OK));
            PluginRepresentation returnedRepresentation = response.getEntity(PluginRepresentation.class);
            assertThat(returnedRepresentation.isEnabled(), is(false));
        }
        finally
        {
            // re-enable plugin, other tests expects it to be enabled
            restTester.enablePlugin(BUNDLED);
        }
    }

    @Test
    public void disableSystemPluginReturnsOK() throws URISyntaxException
    {
        try
        {
            ClientResponse response = restTester.disablePlugin(SYSTEM);
            assertThat(response.getResponseStatus(), is(OK));
        }
        finally
        {
            restTester.enablePlugin(SYSTEM);
        }
    }

    @Test
    public void uninstallNonExistentPluginKeyReturnsNotFound()
    {
        ClientResponse response = restTester.uninstallPlugin(TEST_NONEXISTENT_PLUGIN_KEY);
        assertThat(response.getResponseStatus(), is(NOT_FOUND));
    }

    @Test
    @TestGroups(excludes = FECRU)
    public void uninstallStaticPluginReturnsForbidden()
    {
        ClientResponse response = restTester.uninstallPlugin(STATIC);
        assertThat(response.getResponseStatus(), is(FORBIDDEN));
        ErrorRepresentation representation = getErrorRepresentationFromResponse(response);
        assertThat(representation.getSubCode(), is("upm.pluginUninstall.error.plugin.is.static"));
    }

    @Test
    public void uninstallBundledPluginReturnsForbidden()
    {
        ClientResponse response = restTester.uninstallPlugin(BUNDLED);
        assertThat(response.getResponseStatus(), is(FORBIDDEN));
        ErrorRepresentation representation = getErrorRepresentationFromResponse(response);
        assertThat(representation.getSubCode(), is("upm.pluginUninstall.error.plugin.is.system"));
    }

    @Test
    public void uninstallSystemPluginReturnsForbidden()
    {
        ClientResponse response = restTester.uninstallPlugin(SYSTEM);
        assertThat(response.getResponseStatus(), is(FORBIDDEN));
        ErrorRepresentation representation = getErrorRepresentationFromResponse(response);
        assertThat(representation.getSubCode(), is("upm.pluginUninstall.error.plugin.is.system"));
    }

    @Test
    public void uninstallPluginThatCanBeUninstalled() throws Exception
    {
        restTester.installPluginAndWaitForCompletion(INSTALLABLE.getDownloadUri(restTester.getBaseUri()));

        ClientResponse response = restTester.uninstallPlugin(INSTALLABLE);
        assertThat(response.getResponseStatus(), is(OK));
    }

    @Test
    public void uninstallPluginReturnsConflictWhenInSafeMode() throws JSONException
    {
        restTester.installPluginAndWaitForCompletion(INSTALLABLE.getDownloadUri(restTester.getBaseUri()));

        restTester.enterSafeMode();
        try
        {
            ClientResponse response = restTester.uninstallPlugin(INSTALLABLE);
            assertThat(response.getResponseStatus(), is(CONFLICT));
        }
        finally
        {
            restTester.exitSafeModeAndRestoreSavedConfiguration();
            restTester.uninstallPluginAndVerify(INSTALLABLE);
        }
    }

    @Ignore("PLUG-572 - Bundled plugin wouldn't start correctly after the bundled plugin upgrade was uninstalled")
    public void uninstallingBundledPluginUpgradeCorrectlyStartsBundledPlugin() throws JSONException
    {
        String oldVersion = restTester.getPlugin(UPGRADABLE_BUNDLED).getVersion();

        // install upgrade
        restTester.installPluginAndWaitForCompletion(UPGRADABLE_BUNDLED.getDownloadUri(restTester.getBaseUri()));
        String newVersion = restTester.getPlugin(UPGRADABLE_BUNDLED).getVersion();

        // make sure that the upgrade was successful by checking that the version number changed
        // I have to test it here instead of a separate test because of PLUG-572, once we uninstall the upgrade,
        // the bundled plugin will disappear. This is the only test that uses the UPGRADABLE_BUNDLED
        assertThat(newVersion, is(not(equalTo(oldVersion))));

        // uninstall our upgrade
        restTester.uninstallPlugin(UPGRADABLE_BUNDLED);

        // TODO : PLUG-572 - Bundled plugin wouldn't start correctly after the bundled plugin upgrade was uninstalled
        ClientResponse response = restTester.getPlugin(UPGRADABLE_BUNDLED, ClientResponse.class);
        checkState(response.getResponseStatus() != NOT_FOUND, "Bundled plugin with key: " + UPGRADABLE_BUNDLED + " failed to start");
    }

    @Test
    public void verifyBundledNonSystemPluginRepresentationHasUserInstalledFalse()
    {
        PluginRepresentation plugin = restTester.getPlugin(BUNDLED);
        assertFalse(plugin.isUserInstalled());
    }

    @Test
    public void verifySystemPluginRepresentationHasUserInstalledFalse()
    {
        PluginRepresentation plugin = restTester.getPlugin(SYSTEM);
        assertFalse(plugin.isUserInstalled());
    }

    @Test
    @TestGroups(excludes = FECRU)
    public void verifyStaticPluginRepresentationHasUserInstalledTrue()
    {
        PluginRepresentation plugin = restTester.getPlugin(STATIC);
        assertTrue(plugin.isUserInstalled());
    }

    @Test
    public void verifyUserInstalledPluginRepresentationHasUserInstalledTrue() throws JSONException
    {
        try
        {
            restTester.installPluginAndWaitForCompletion(INSTALLABLE.getDownloadUri(restTester.getBaseUri()));
            PluginRepresentation plugin = restTester.getPlugin(INSTALLABLE);
            assertTrue(plugin.isUserInstalled());
        }
        finally
        {
            restTester.uninstallPluginAndVerify(INSTALLABLE);
        }
    }

    @Test
    public void verifyUserInstalledPluginRepresentationHasOptionalTrue() throws JSONException
    {
        try
        {
            restTester.installPluginAndWaitForCompletion(INSTALLABLE.getDownloadUri(restTester.getBaseUri()));
            PluginRepresentation plugin = restTester.getPlugin(INSTALLABLE);
            assertTrue(plugin.isOptional());
        }
        finally
        {
            restTester.uninstallPluginAndVerify(INSTALLABLE);
        }
    }

    @Test
    public void verifyInstalledPluginRepresentationIsConfigurable() throws JSONException
    {
        try
        {
            restTester.installPluginAndWaitForCompletion(INSTALLABLE_WITH_CONFIG.getDownloadUri(restTester.getBaseUri()));
            PluginRepresentation plugin = restTester.getPlugin(INSTALLABLE_WITH_CONFIG);
            assertTrue(plugin.isConfigurable());
        }
        finally
        {
            restTester.uninstallPluginAndVerify(INSTALLABLE_WITH_CONFIG);
        }
    }

    @Test
    public void verifyBundledPluginRepresentationIsConfigurable()
    {
        PluginRepresentation plugin = restTester.getPlugin(BUNDLED_WITH_CONFIG);
        assertTrue(plugin.isConfigurable());

        plugin = restTester.getPlugin(BUNDLED);
        assertFalse(plugin.isConfigurable());
    }

    @Test
    public void installedPluginThatRequiresRestartHasCorrectRestartState() throws Exception
    {
        restTester.installPluginAndWaitForCompletion(INSTALLABLE_REQUIRES_RESTART);
        try
        {
            PluginRepresentation plugin = restTester.getPlugin(INSTALLABLE_REQUIRES_RESTART);
            assertThat(plugin.getRestartState(), is(equalTo("install")));
        }
        finally
        {
            restTester.deleteChangeRequiringRestart(INSTALLABLE_REQUIRES_RESTART);
        }
    }

    @Test
    public void assertThatUpgradingPluginThatRequiresRestartResultsInItHavingRestartStateAsUpgrade() throws Exception
    {
        ClientResponse response = restTester.installPluginAndWaitForCompletion(UPGRADEABLE_REQUIRES_RESTART_V2);
        try
        {
            PluginRepresentation plugin = response.getEntity(PluginRepresentation.class);
            assertThat(plugin.getRestartState(), is(equalTo("upgrade")));
        }
        finally
        {
            restTester.deleteChangeRequiringRestart(UPGRADEABLE_REQUIRES_RESTART_V2);
        }
    }

    @Test
    public void assertThatPluginsRequiringRestartHaveLinkToChange() throws Exception
    {
        restTester.installPluginAndWaitForCompletion(INSTALLABLE_REQUIRES_RESTART);
        try
        {
            PluginRepresentation plugin = restTester.getPlugin(INSTALLABLE_REQUIRES_RESTART);
            assertThat(plugin.getChangeRequiringRestartLink().toASCIIString(), endsWith("/plugins/1.0/requires-restart/" + INSTALLABLE_REQUIRES_RESTART.getEscapedKey()));
        }
        finally
        {
            restTester.deleteChangeRequiringRestart(INSTALLABLE_REQUIRES_RESTART);
        }
    }

    /**
     * This test cannot be run in Refapp because it needs to run in an application with a home directory and real user
     * installed plugins.  Refapp doesn't have a home directory so user installed plugins specified in the amps plugin
     * get lumped in as bundled plugins and so can't be uninstalled.
     */
    @Test
    @TestGroups(excludes = {REFAPP,CONFLUENCE})
    public void assertThatUninstallingPluginThatRequiresRestartReturnsAcceptedResponse()
    {
        ClientResponse response = restTester.uninstallPlugin(UPGRADEABLE_REQUIRES_RESTART_V1);
        try
        {
            assertThat(response, is(accepted()));
        }
        finally
        {
            restTester.deleteChangeRequiringRestart(UPGRADEABLE_REQUIRES_RESTART_V1);
        }
    }
}
