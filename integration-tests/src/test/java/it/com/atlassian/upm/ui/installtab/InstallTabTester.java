package it.com.atlassian.upm.ui.installtab;

import java.io.File;
import java.io.IOException;
import java.net.URI;

import com.atlassian.integrationtesting.ApplicationPropertiesImpl;
import com.atlassian.upm.rest.UpmUriBuilder;
import com.atlassian.upm.test.TestPlugins;
import com.atlassian.upm.test.UpmUiTester;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlFileInput;
import com.gargoylesoftware.htmlunit.html.HtmlSelect;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;

import org.junit.Ignore;

import be.roam.hue.doj.Doj;

import static com.atlassian.upm.test.TestPlugins.INSTALLABLE;
import static com.atlassian.upm.test.TestPlugins.NOT_ENABLED_BY_DEFAULT;
import static com.google.common.base.Preconditions.checkState;

@Ignore
public class InstallTabTester
{
    private static final UpmUriBuilder uriBuilder = new UpmUriBuilder(ApplicationPropertiesImpl.getStandardApplicationProperties());

    private final UpmUiTester uiTester;

    public InstallTabTester(UpmUiTester uiTester)
    {
        this.uiTester = uiTester;
    }

    public Doj getPluginElement(PluginInstallType type)
    {
        return getPluginElement(type.type(), type.key());
    }

    public Doj getPluginElement(String section, String pluginKey)
    {
        return uiTester.elementById("upm-install-" + section).get("input").withValue(uriBuilder.buildAvailablePluginUri(pluginKey).toString()).parent();
    }

    public Doj getInstalledPluginElement(TestPlugins plugin)
    {
        return uiTester.elementById("upm-panel-manage").get("input").withValue(uriBuilder.buildPluginUri(plugin.getKey()).toString()).parent();
    }

    /**
     * Selects the specified type of installable plugins to display from the dropdown
     *
     * @param type the type of install to select
     * @throws IOException if the 'click' failed
     * @throws ElementNotFoundException if the dropdown did not contain the requested type
     */
    public void select(PluginInstallType type) throws IOException, ElementNotFoundException
    {
        HtmlSelect select = getInstallTypeSelector();
        checkState(select != null, "Cannot find install type dropdown in current page");
        select.focus();
        select.getOptionByValue(type.type()).click();
        uiTester.waitForAsyncEventsThatBeginWithinDefaultTime();
    }

    public void expandPluginDetails(PluginInstallType type) throws Exception
    {
        getPluginElement(type).get("div.upm-plugin-row").click();
        uiTester.waitForAsyncEventsThatBeginWithinDefaultTime();
    }

    public void expandPluginDetails(String section, TestPlugins plugin) throws Exception
    {
        getPluginElement(section, plugin.getKey()).get("div.upm-plugin-row").click();
        uiTester.waitForAsyncEventsThatBeginWithinDefaultTime();
    }

    public void clickInstall(PluginInstallType type) throws Exception
    {
        getPluginElement(type).get("button.upm-install").click();
        uiTester.waitForAsyncEventsThatBeginWithinDefaultTime();
    }

    public void clickUploadButton()
    {
        uiTester.clickElementWithId("upm-upload");
    }

    public HtmlElement getUploadDialog()
    {
        return uiTester.getElementById("upm-upload-dialog");
    }

    public HtmlSelect getInstallTypeSelector()
    {
        return (HtmlSelect) uiTester.getElementById("upm-install-type");
    }

    /**
     * Returns the names of plugins that can be installed from the featured selection.
     *
     * @return the names of plugins that can be installed from the featured selection.
     */
    public Iterable<String> getVisibleFeaturedPlugins()
    {
        return uiTester.getVisiblePluginsForId("upm-install-featured");
    }

    /**
     * Returns the names of plugins that can be installed from the popular selection.
     *
     * @return the names of plugins that can be installed from the popular selection.
     */
    public Iterable<String> getVisiblePopularPlugins()
    {
        return uiTester.getVisiblePluginsForId("upm-install-popular");
    }

    /**
     * Returns the names of all plugins available for install.
     *
     * @return the names of all plugins available for install.
     */
    public Iterable<String> getVisibleAvailablePlugins()
    {
        return uiTester.getVisiblePluginsForId("upm-install-available");
    }

    public enum PluginInstallType
    {
        available("available", INSTALLABLE.getKey()),
        popular("popular", INSTALLABLE.getKey()),
        featured("featured", "com.atlassian.jira.ext.charting"),
        availableNotEnabled("available", NOT_ENABLED_BY_DEFAULT.getKey());

        private final String type, key;

        PluginInstallType(String type, String key)
        {
            this.type = type;
            this.key = key;
        }

        public String type()
        {
            return type;
        }

        public String key()
        {
            return key;
        }
    }

    public void uploadFile(File pluginFile) throws Exception
    {
        clickUploadButton();

        HtmlFileInput input = (HtmlFileInput) uiTester.getElementById("upm-upload-file");
        input.setValueAttribute(pluginFile.getAbsolutePath());

        uiTester.elementById("upm-upload-dialog").get("button").withTextMatching("Upload").click();
        uiTester.waitForAsyncEventsThatBeginWithinDefaultTime();
    }

    public void uploadFromUri(URI pluginUri) throws Exception
    {
        clickUploadButton();

        HtmlTextInput input = (HtmlTextInput) uiTester.getElementById("upm-upload-url");
        input.setValueAttribute(pluginUri.toASCIIString());

        uiTester.elementById("upm-upload-dialog").get("button").withTextMatching("Upload").click();
        uiTester.waitForAsyncEventsThatBeginWithinDefaultTime();
    }
    
    public URI getBaseUri()
    {
        return URI.create(ApplicationPropertiesImpl.getStandardApplicationProperties().getBaseUrl());
    }
}
